import React from 'react'
import './footer.css'
import {RiLinkedinFill} from 'react-icons/ri'
import {FaFacebookF} from 'react-icons/fa'
import {AiFillInstagram} from 'react-icons/ai'


const footer = () => {
  return (
    <footer>
    <a href="#" className="footer_logo"> JESTAN</a>
    
    <ul className='permalinks'>
         
        <li><a href="#">Home</a></li>
        <li><a href="#about">About</a></li>
        <li><a href="#experience">Experience</a></li>
        <li><a href="#services">Services</a></li>
        <li><a href="#portfolio">Portfolio</a></li>
        <li><a href="#testimonials">Testimonials</a></li>
        <li><a href="#contact">Contact</a></li>

    </ul>

    

    <div className='footer_socials'>
        <a href="https://facebook.com"><FaFacebookF/></a>
        <a href="https://instagram.com"><AiFillInstagram/></a>
        <a href="https://twitter.com"><RiLinkedinFill/></a>
    </div>
    

   <div className="footer_copyright"></div>
     <small>&copy; Jestan Dale Mendame | All Rights Reserved</small>

    </footer>
  )
}

export default footer
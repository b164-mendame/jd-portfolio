import React from 'react'
import './testimonials.css'
import AVTR1 from '../../assets/avatar1.jpg'
import AVTR2 from '../../assets/avatar2.jpg'
import AVTR3 from '../../assets/avatar3.jpg'
import AVTR4 from '../../assets/avatar4.jpg'
// import Swiper core and required modules
import {Pagination} from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';


const data = [

{
avatar: AVTR1,
name: "Tina Snow",
review: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolorum illo ducimus quo amet commodi repudiandae ipsa dolorem possimus voluptate nemo!"
},


{
  avatar: AVTR2,
  name: "Shatta Wale",
  review: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolorum illo ducimus quo amet commodi repudiandae ipsa dolorem possimus voluptate nemo!"
},


{
    avatar: AVTR3,
    name: "John Stephenson",
    review: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dicta, sed, commodi saepe at corporis tenetur quos accusantium temporibus facere culpa sapiente ducimus architecto nisi voluptas maiores quam eos officia facilis!"
},



{
  avatar: AVTR4,
  name: "Stephanie Bellieu",
  review: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolorum illo ducimus quo amet commodi repudiandae ipsa dolorem possimus voluptate nemo!"
},

]

const Testimonials = () => {
  return (
    <section id="testimonials">
      <h5>Satisfied Clients</h5>
      <h2>Testimonials</h2>

       <Swiper className="container testimonials_container"
             // install Swiper modules
      modules={[Pagination]}
      spaceBetween={40}
      slidesPerView={1}
      pagination={{ clickable: true }}>

        {
         data.map(({avatar,name,review}, index) =>

         <SwiperSlide key={index} className='testimonial'>
        <div className="client_avatar">
          <img src={avatar} alt="Avatar One" />   
        </div>
        <h5 className='client_name'>{name}</h5>
           <small className='client_review'>{review}</small>     
        </SwiperSlide>

         )
        }

       </Swiper>
    </section>
  )
}

export default Testimonials